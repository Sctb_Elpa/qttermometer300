#include <stdexcept>
#include <stddef.h>

#include <mylog/mylog.h>
#include <myusbhiddevice/usbhiddevice.h>

#include "devisesettingsmodel.h"

#include "Termometer300.h"

struct MeteostationOutput
{
    float Temperature;
    float F_temperature;
} __attribute__((packed));


Termometer300::Termometer300(QObject *parent) :
    QObject(parent)
{
    device = new UsbHidDevice(SCTB_ELPA_VID, TERMOMETER300_PID, this);
    comonConstructor();
}

Termometer300::Termometer300(libusb_device *dev, QObject *parent)
{
    device = new UsbHidDevice(dev, this);
    comonConstructor();
}

Termometer300::Mesure Termometer300::getLastMesure() const
{
    return lastMesure;
}

DeviceSettingsModel *Termometer300::settingsModel() const
{
    return settingsmodel;
}

UsbHidDevice *Termometer300::getDevice() const
{
    return device;
}

void Termometer300::getRawMesure(const QByteArray &data)
{
    if (data.size() == sizeof(MeteostationOutput))
    {
        MeteostationOutput * report = (MeteostationOutput*)data.data();

        Mesure result =
        {
            QDateTime::currentDateTime(),
            report->Temperature,
            report->F_temperature,
        };
/*
        LOG_DEBUG(trUtf8("Timestamp: %1\n\tPressure = %2\n\tTemperature = %3\n\tHumidity = %4").arg(
                      result.timestamp.toString(Qt::ISODate)).arg(
                      result.Pressure).arg(
                      result.Temperature).arg(
                      result.Humidity));
*/
        lastMesure = result;

        emit dataRessived(result);
    }
}


void Termometer300::comonConstructor()
{
    settingsmodel = new DeviceSettingsModel(this);

    connect(device, SIGNAL(IntDataRessived(QByteArray)), this, SLOT(getRawMesure(QByteArray)));
    if (!device->open(QIODevice::ReadWrite | QIODevice::Unbuffered))
        throw std::runtime_error("Failed to open device.");
}
