#include "mycontainer.h"

myContainer::myContainer(const QVariant &data, QObject *parent) :
    QObjectUserData()
{
    d = data;
}

QVariant myContainer::data() const
{
    return d;
}

myContainer::~myContainer()
{
    //LOG_DEBUG("~myContainer()");
}
