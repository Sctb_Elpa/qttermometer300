#include <math.h>

#include <QTableWidget>
#include <QHeaderView>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFile>
#include <QFileDialog>
#include <QTextCodec>
#include <QTreeView>

#include <mylog.h>
#include <settingscmdline.h>

#include "itemdelegate.h"
#include "Graph.h"
#include "stocktimescaledraw.h"

#include "realtimegraphwindow.h"

RealtimeGraphWindow::RealtimeGraphWindow(Termometer300 *meteostation, QWidget *parent) :
    QSplitter(Qt::Horizontal, parent)
{
    m = meteostation;
    resfile = NULL;
    codec = QTextCodec::codecForName("Windows-1251");

    graph = new Graph;

    graph->enableAxis(QwtPlot::yLeft, true);
    graph->enableAxis(QwtPlot::xBottom, true);
    graph->setAxisScaleDraw(QwtPlot::xBottom, new StockTimeScaleDraw(trUtf8("mm.ss")));
    //graph->setStyleSheet("background-color: qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, stop:0 #E8E1B7, stop:1 #B7BEE8);");
    graph->setStyleSheet(styleSheet());

    table = new QTableWidget(4, 1);
    table->setHorizontalHeaderLabels(QStringList() << trUtf8("Значение"));
    table->setVerticalHeaderLabels(QStringList() << "T" << "Ft" << trUtf8("Итервал опроса") << trUtf8("Кол-во точек"));

    table->model()->setData(table->model()->index(2, 0), SettingsCmdLine::settings->value("realtime-interval"));
    table->model()->setData(table->model()->index(3, 0), SettingsCmdLine::settings->value("realtime-history-len"));

    table->horizontalHeader()->setStretchLastSection(true);

    connect(table->model(), SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(editFinished(QModelIndex,QModelIndex)));

    QWidget *w = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(table);

    savebtn = new QPushButton(QIcon(":/res/media_record.png"), trUtf8("Запись"));
    savebtn->setCheckable(true);
    connect(savebtn, SIGNAL(clicked(bool)), this, SLOT(savingTrigered(bool)));

    QPushButton* settingsButton = new QPushButton(QIcon(":/res/Tools1.png"), trUtf8("Настройки"));
    connect(settingsButton, SIGNAL(clicked()), this, SLOT(showSettingsWindow()));

    layout->addWidget(savebtn);
    layout->addWidget(settingsButton);
    layout->addStretch();

    w->setLayout(layout);

    addWidget(graph);
    addWidget(w);

    setStretchFactor(0, 1);
    setStretchFactor(1, 0);
}

RealtimeGraphWindow::~RealtimeGraphWindow()
{
    savingTrigered(false);
}

void RealtimeGraphWindow::UpdateGraph()
{
    QVector<QPointF> values[2];

    foreach(Termometer300::Mesure mesure, history)
    {
        double timestamp = QDateTime().msecsTo(mesure.timestamp);
        values[0].append(QPointF(timestamp, mesure.Temperature));
        //values[1].append(QPointF(timestamp, mesure.F_temperature)); // не нужно
    }

    graph->SetData(values);
}

void RealtimeGraphWindow::editFinished(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    if ((topLeft.row() == bottomRight.row()) &&
            (topLeft.column() == bottomRight.column()))
        switch(topLeft.row())
        {
        case 2: //  Интервал обновления
        {
            bool ok;
            float value = table->model()->data(topLeft).toFloat(&ok);

            if (!ok || value <= 0)
            {
                QMessageBox::critical(this, trUtf8("Ошибка ввода"), trUtf8("Разрешены только положительные числа"), QMessageBox::Ok);
                return;
            }
            (*SettingsCmdLine::settings)["realtime-interval"] = value;
            break;
        }
        case 3: //  Длина истории
        {
            bool ok;
            unsigned int value = table->model()->data(topLeft).toUInt(&ok);

            if (!ok)
            {
                QMessageBox::critical(this, trUtf8("Ошибка ввода"), trUtf8("Разрешены только целые положительные числа"), QMessageBox::Ok);
                return;
            }
            (*SettingsCmdLine::settings)["realtime-history-len"] = value;
            break;
        }
        }
}

void RealtimeGraphWindow::savingTrigered(bool save)
{
    if (resfile)
    {
        resfile->close();
        delete resfile;
        resfile = NULL;
    }

    if (save)
    {
        QString filename = QFileDialog::getSaveFileName(this, trUtf8("Записать в.."), QString(), QObject::trUtf8("CSV table (*.csv)"));

        resfile = new QFile(filename);
        if (filename.isEmpty() || (!resfile->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)))
        {
            savebtn->blockSignals(true);
            savebtn->setChecked(false);
            savebtn->blockSignals(false);
            delete resfile;
            return;
        }

        //header
        QString header = trUtf8("Дата;Время;Температура [°C];Частота температуры [Hz];\n");

        if (resfile->size() != 0)
            resfile->write("\n");

        resfile->write(codec->fromUnicode(header));
    }
}

void RealtimeGraphWindow::showSettingsWindow()
{
    if (m)
    {
        QTreeView* table = new QTreeView;
        table->setWindowTitle(trUtf8("Настройки"));
        ItemDelegate *delegate = new ItemDelegate(table);
        table->setModel((QAbstractItemModel*)m->settingsModel());
        table->setItemDelegate(delegate);
        table->setEditTriggers(QAbstractItemView::CurrentChanged);
        table->setAttribute(Qt::WA_DeleteOnClose, true);
        table->show();
    }
}

void RealtimeGraphWindow::newMesure(const Termometer300::Mesure &mesure)
{
    if (lastWriten.msecsTo(mesure.timestamp) > SettingsCmdLine::settings->value("realtime-interval").toFloat() * 1000)
    {
        lastWriten = mesure.timestamp;

        history.append(mesure);
        int maxhistorysize = SettingsCmdLine::settings->value("realtime-history-len").toInt();
        if (history.size() > maxhistorysize)
            history.remove(0,  history.size() - maxhistorysize);

        UpdateGraph();

        table->model()->setData(table->model()->index(0, 0), mesure.Temperature);
        table->model()->setData(table->model()->index(1, 0), mesure.F_temperature);

        if (resfile)
        {
            QByteArray res;
            res.append(codec->fromUnicode(mesure.timestamp.date().toString()));
            res.append(';');
            res.append(codec->fromUnicode(mesure.timestamp.time().toString()));
            res.append(';');
            res.append(QString::number(mesure.Temperature) + ";");
            res.append(QString::number(mesure.F_temperature) + ";");

            res[res.size() - 1] = '\n';
            resfile->write(res);
        }
    }
}
