#include <cstdio>
#include <iostream>

#include <QIODevice>

#include <mylog/mylog.h>

#include "Settings.h"

/// Типы ключей
enum enArgs
{
    OPT_HELP,
};

/// Таблица ключей программы
static CSimpleOpt::SOption g_rgOptions[] =
{
    {
        OPT_HELP, _T("-h"), SO_NONE
    }, /// "-h"
    {
        OPT_HELP, _T("--help"), SO_NONE
    }, /// "--help"
    SO_END_OF_OPTIONS
    // END
};

/// Таблица указателей
SettingsCmdLine::pfTable Settings::table =
{
    g_rgOptions,
    Settings::SetDefaultValues,
    Settings::console_ShowUsage,
    Settings::parse_Arg
};

void Settings::console_ShowUsage(cSettingsCmdLine* _this)
{
    std::cout
            << trUtf8(
                   "Usage: Termometer300 [-h | --help]\n\
                   \n").toLatin1().data();
}

struct SettingsCmdLine::key_val_res Settings::parse_Arg(int optCode,
          const char* optText, char *ArgVal, cSettingsCmdLine* origins)
{
    SettingsCmdLine::key_val_res result =
    {
            QString(), QVariant(), true
    };
    switch (optCode)
    {
    default:
        result.res = false;
        break;
    }
    return result;
}

void Settings::SetDefaultValues(cSettingsCmdLine* _this)
{
    _this->insert("realtime-history-len", __DEFAULT_REALTIME_HISTORY_LEN);
    _this->insert("realtime-interval", 0);
}

Settings::Settings(int argc, char *argv[]) :
    QObject(NULL), cSettingsCmdLine(argc, argv, "res/Settings.ini", &table)
  /// параметр 3 устанавливает имя файла настроек программы
{
}
