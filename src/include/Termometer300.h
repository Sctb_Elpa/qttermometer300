#ifndef METEOSTATION_H
#define METEOSTATION_H

#include <QDateTime>
#include <QObject>
#include <QMap>

#include <usbhiddevice.h>

#define SCTB_ELPA_VID               (0x03EB)
#define TERMOMETER300_PID           (0x204E)

#define USB_PULL_INTERVAL           (100)

#define EXT_EEPROM_SIZE             (262144 / 8)
#define EXT_EEPROM_PAGE_SIZE        (64)


enum enMemoryRegion
{
    SETTINGS    = 0x01 << 16
};

class DeviceSettingsModel;

class Termometer300 : public QObject
{
    Q_OBJECT
public:
    struct Mesure
    {
        QDateTime timestamp;
        float Temperature;
        float F_temperature;
    };

    explicit Termometer300(QObject *parent = 0);
    explicit Termometer300(libusb_device *dev, QObject *parent = NULL);

    Mesure getLastMesure() const;

    DeviceSettingsModel *settingsModel() const;
    UsbHidDevice* getDevice() const;
    
signals:
    void dataRessived(const Termometer300::Mesure& mesure);

private:
    Mesure lastMesure;

    UsbHidDevice* device;

    DeviceSettingsModel *settingsmodel;

    void comonConstructor();

private slots:
    void getRawMesure(const QByteArray& data);
};

#endif // METEOSTATION_H
