#ifndef REALTIMEGRAPHWINDOW_H
#define REALTIMEGRAPHWINDOW_H

#include <QSplitter>
#include <QVector>
#include <QDateTime>

#include "Termometer300.h"

class Graph;
class QTableWidget;
class QModelIndex;
class QPushButton;
class QFile;
class Termometer300;

class RealtimeGraphWindow : public QSplitter
{
    Q_OBJECT
public:
    explicit RealtimeGraphWindow(Termometer300 *meteostation, QWidget *parent = 0);
    ~RealtimeGraphWindow();

private:
    Graph* graph;
    QTableWidget* table;
    QVector<Termometer300::Mesure> history;
    QDateTime lastWriten;
    QPushButton* savebtn;
    QFile* resfile;
    QTextCodec *codec;
    Termometer300* m;

    void UpdateGraph();

private slots:
    void editFinished(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void savingTrigered(bool save);
    void showSettingsWindow();

public slots:
    void newMesure(const Termometer300::Mesure& mesure);
};

#endif // REALTIMEGRAPHWINDOW_H
