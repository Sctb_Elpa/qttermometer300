#ifndef GRARAPHTOOLTIP_H
#define GRARAPHTOOLTIP_H

#include <QVector>
#include <QModelIndex>

#include <qwt_plot.h>

class QwtPlot;
class QModelIndex;
class SplineCurve;

class Graph : public QwtPlot
{
    Q_OBJECT
public:
    explicit Graph(QModelIndex *index = NULL);
    ~Graph();

    QModelIndex* index() const;

    void SetData(const QVector<QPointF> TPH[3]);

private:
    QModelIndex* referenceIndex;
    QVector< SplineCurve* > curves;

    static SplineCurve* buildCurve(const QString& name, const QColor& color, const int style, const int y_axis);
};

#endif // GRARAPHTOOLTIP_H
