/****************************************************************************
 **
 ** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
 ** Contact: http://www.qt-project.org/legal
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:BSD$
 ** You may use this file under the terms of the BSD license as follows:
 **
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
 **     of its contributors may be used to endorse or promote products derived
 **     from this software without specific prior written permission.
 **
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 **
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

#ifndef DEVISESETTINGSMODEL_H
#define DEVISESETTINGSMODEL_H

#include <QAbstractTableModel>
#include <QStringList>
#include <QModelIndex>
#include <QVariant>
#include <stdint.h>


struct sTemperatureCoeffs
{
    float T0;
    float F0;
    float C[3];
};

struct sPressureCoeffs
{
    float Ft0;
    float Fp0;
    float A[6];
};

struct sWriteIntervalVariants
{
    uint16_t	  variants[3];
    uint8_t		  selectd;
} __attribute__((packed));

struct sSettings
{
    // коэфцициенты для расчета температуры
    struct sTemperatureCoeffs		TemperatureCoeffs;

    // Время изменерения
    float						MesureTime;

    // показывать частоту вместо температуры
    uint8_t						DisplyaFreq;
} __attribute__((packed));


class Termometer300;
class TreeItem;

class DeviceSettingsModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit DeviceSettingsModel(Termometer300 *parent);
    virtual ~DeviceSettingsModel();

    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;
    virtual QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &index) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

    enum enSettingsVarType
    {
        FLOAT,
        UINT8_T,
        INT8_T,
        UINT16_T,
        BOOL
    };

    struct memoryMapItem
    {
        QString path;
        QString name;
        QString desctription;
        qint64 offsset;
        enum enSettingsVarType type;
        int size;
        QString groupName;
    };

    QVariant value(memoryMapItem* item) const;

    void forcerereadData(const QModelIndex &index);

private:
    QMap<QString, const memoryMapItem*> settingsMap;
    QStringList keys;

    mutable sSettings cashedSettings;

    void tryCashValues() const;

    mutable bool valuesCashed;

    TreeItem *rootItem;

    void setupModelData(TreeItem *root);
};

DeviceSettingsModel::memoryMapItem* getmemoryMapItem(const QModelIndex &index);

#endif // DEVISESETTINGSMODEL_H
