#ifndef MYCONTAINER_H
#define MYCONTAINER_H

#include <QObjectUserData>
#include <QVariant>

class myContainer : public QObjectUserData
{
public:
    myContainer(const QVariant& data, QObject *parent = NULL);
    ~myContainer();
    QVariant data() const;

private:
    QVariant d;
};

#endif // MYCONTAINER_H
