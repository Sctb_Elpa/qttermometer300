#include <QDateTime>

#include "stocktimescaledraw.h"

StockTimeScaleDraw::StockTimeScaleDraw(const QString& fmt) :
    format(fmt)
{
}

QwtText StockTimeScaleDraw::label(double v) const
{
    QDateTime t = QDateTime().addMSecs((qint64)v);
    return t.toString(format);
}


