#include <QSpinBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QIODevice>
#include <QStylePainter>
#include <QVariant>

#include <mylog.h>

#include "treeitem.h"
#include "devisesettingsmodel.h"

#include "itemdelegate.h"

ItemDelegate::ItemDelegate(QObject *parent) :
    QItemDelegate(parent)
{
}

void ItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (index.isValid() && (index.column() == 1))
    {
        DeviceSettingsModel::memoryMapItem* item = getmemoryMapItem(index);
        QVariant value = ((DeviceSettingsModel*)index.model())->value(item);
        if (item && item->type == DeviceSettingsModel::BOOL)
        {
            QWidget * w = dynamic_cast<QWidget *>( painter->device() );
            if ( w )
            {
                QStylePainter p( w );
                QStyleOptionButton opt;
                opt.initFrom( w );
                opt.rect = option.rect;
                opt.state |= value.toBool() ? QStyle::State_On : QStyle::State_Off;
                opt.text = "";
                p.drawControl( QStyle::CE_CheckBox, opt );
                return;
            }
        }
    }
    QItemDelegate::paint(painter, option, index);
}

QWidget *ItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (index.column() == 1)
    {
        DeviceSettingsModel::memoryMapItem* item = getmemoryMapItem(index);
        if (item)
        {
            DeviceSettingsModel::enSettingsVarType type = item->type;
            switch(type)
            {
            case DeviceSettingsModel::FLOAT:
                return QItemDelegate::createEditor(parent, option, index); // стандартный эдитор для флотов
            case DeviceSettingsModel::UINT8_T:
            {
                QSpinBox *res = new QSpinBox(parent);
                res->setMaximum(255);
                res->setMinimum(0);
                return res;
            }
            case DeviceSettingsModel::INT8_T:
            {
                QSpinBox *res = new QSpinBox(parent);
                res->setMaximum(-127);
                res->setMinimum(127);
                return res;
            }
            case DeviceSettingsModel::UINT16_T:
            {
                QSpinBox *res = new QSpinBox(parent);
                res->setMaximum(0xffff);
                res->setMinimum(0);
                return res;
            }
            case DeviceSettingsModel::BOOL:
                return new QCheckBox(parent);
            }
        }
    }
    else
        return NULL;
}

void ItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if (index.column() == 1)
    {
        DeviceSettingsModel::memoryMapItem* item = getmemoryMapItem(index);
        QVariant value = ((DeviceSettingsModel*)index.model())->value(item);
        switch(item->type)
        {
        case DeviceSettingsModel::FLOAT:
            QItemDelegate::setEditorData(editor, index);
            break;
        case DeviceSettingsModel::UINT8_T:
        case DeviceSettingsModel::UINT16_T:
            ((QSpinBox*)editor)->setValue(value.toUInt());
            break;
        case DeviceSettingsModel::INT8_T:
            ((QSpinBox*)editor)->setValue(value.toInt());
            break;
        case DeviceSettingsModel::BOOL:
            ((QCheckBox*)editor)->setChecked(!value.toBool()); // специально инверсия
            break;
        }
    }
    else
        QItemDelegate::setEditorData(editor, index);
}

void ItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if (index.column() == 1)
    {
        bool ok;
        QVariant r;
        QString editorType(editor->metaObject()->className());

        if (editorType == "QExpandingLineEdit")
        {
            QLineEdit* e = (QLineEdit*)editor;
            float result = e->text().toFloat(&ok);
            if (ok)
                r = result;
        }
        else if (editorType == "QSpinBox")
        {
            QSpinBox* e = (QSpinBox*)editor;
            ok = true;
            r = e->value();
        }
        else if (editorType == "QCheckBox")
        {
            QCheckBox* e = (QCheckBox*)editor;
            ok = true;
            r = e->isChecked();
        }
        if (ok)
            model->setData(index, r, Qt::EditRole);
    }
    else
        QItemDelegate::setModelData(editor, model, index);
}

void ItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (index.column() == 1)
        editor->setGeometry(option.rect);
    else
        QItemDelegate::updateEditorGeometry(editor, option, index);
}

