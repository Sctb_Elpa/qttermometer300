#include <QStringList>

#include <mylog/mylog.h>

#include "Termometer300.h"
#include "treeitem.h"

#include "devisesettingsmodel.h"

static const DeviceSettingsModel::memoryMapItem memoryMap[] =
{
    {
        QObject::trUtf8("TemperatureCoeffs/T0"),
        QObject::trUtf8("T0"),
        QObject::trUtf8("Коэфициент T0 для расчета T(Ft)"),
        SETTINGS | offsetof(sSettings, TemperatureCoeffs.T0),
        DeviceSettingsModel::FLOAT, sizeof(float),
        QObject::trUtf8("Коэфициенты для расчета температуры")
    },
    {
        QObject::trUtf8("TemperatureCoeffs/F0"),
        QObject::trUtf8("F0"),
        QObject::trUtf8("Коэфициент F0 для расчета T(Ft)"),
        SETTINGS | offsetof(sSettings, TemperatureCoeffs.F0),
        DeviceSettingsModel::FLOAT, sizeof(float),
        QObject::trUtf8("Коэфициенты для расчета температуры")
    },
    {
        QObject::trUtf8("TemperatureCoeffs/C0"),
        QObject::trUtf8("C[0]"),
        QObject::trUtf8("Коэфициент C[0] для расчета T(Ft)"),
        SETTINGS | offsetof(sSettings, TemperatureCoeffs.C[0]),
        DeviceSettingsModel::FLOAT, sizeof(float),
        QObject::trUtf8("Коэфициенты для расчета температуры")
    },
    {
        QObject::trUtf8("TemperatureCoeffs/C1"),
        QObject::trUtf8("C[1]"),
        QObject::trUtf8("Коэфициент C[1] для расчета T(Ft)"),
        SETTINGS | offsetof(sSettings, TemperatureCoeffs.C[1]),
        DeviceSettingsModel::FLOAT, sizeof(float),
        QObject::trUtf8("Коэфициенты для расчета температуры")
    },
    {
        QObject::trUtf8("TemperatureCoeffs/C2"),
        QObject::trUtf8("C[2]"),
        QObject::trUtf8("Коэфициент C[2] для расчета T(Ft)"),
        SETTINGS | offsetof(sSettings, TemperatureCoeffs.C[2]),
        DeviceSettingsModel::FLOAT, sizeof(float),
        QObject::trUtf8("Коэфициенты для расчета температуры")
    },
    {
        QObject::trUtf8("MesureTime"),
        QObject::trUtf8("Время измерения [c]"),
        QObject::trUtf8("Время измерения в секундах"),
        SETTINGS | offsetof(sSettings, MesureTime),
        DeviceSettingsModel::FLOAT, sizeof(float),
        QObject::trUtf8("MesureTime")
    },
    {
        QObject::trUtf8("DisplyaFreq"),
        QObject::trUtf8("Показать частоту"),
        QObject::trUtf8("Отобразить на дисплее частоту температуры\nвместо значения температуры"),
        SETTINGS | offsetof(sSettings, DisplyaFreq),
        DeviceSettingsModel::BOOL, sizeof(uint8_t),
        QObject::trUtf8("DisplyaFreq")
    }
};

DeviceSettingsModel::memoryMapItem* getmemoryMapItem(const QModelIndex &index)
{
    DeviceSettingsModel::memoryMapItem* res = NULL;
    if (index.isValid())
    {
        TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
        if (item->data(1).toInt() != -1)
            res = (DeviceSettingsModel::memoryMapItem*)(*((size_t*)item->data(1).toByteArray().data()));
    }
    return res;
}

DeviceSettingsModel::DeviceSettingsModel(Termometer300 *parent) :
    QAbstractTableModel(parent)
{
    valuesCashed = false;

    QList<QVariant> rootData;
    rootData << trUtf8("Параметр") << trUtf8("Значение");
    rootItem = new TreeItem("/", rootData);

    setupModelData(rootItem);
}

DeviceSettingsModel::~DeviceSettingsModel()
{
    delete rootItem;
}

int DeviceSettingsModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

Qt::ItemFlags DeviceSettingsModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsEditable;
}

QVariant DeviceSettingsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    tryCashValues();

    memoryMapItem* MapItem = getmemoryMapItem(index);

    switch(role)
    {
    case Qt::EditRole:
    case Qt::DisplayRole:
        switch (index.column())
        {
        case 0:
            return (static_cast<TreeItem*>(index.internalPointer()))->data(0);
        case 1:
            return value(MapItem);
        }
    case Qt::ToolTipRole:
        if (!MapItem)
            return QVariant();
        else
            return MapItem->desctription;

    default:
        return QVariant();
    }
}

bool DeviceSettingsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    TreeItem *treeItem = static_cast<TreeItem*>(index.internalPointer());
    DeviceSettingsModel::memoryMapItem* MapItem = getmemoryMapItem(index);

    char buff[MapItem->size];

    switch (MapItem->type)
    {
    case FLOAT:
        *((float*)buff) = value.toFloat();
        break;
    case UINT8_T:
        *((uint8_t*)buff) = value.toUInt();
        break;
    case INT8_T:
        *((int8_t*)buff) = value.toInt();
        break;
    case UINT16_T:
        *((uint16_t*)buff) = value.toUInt();
        break;
    case BOOL:
        *((uint8_t*)buff) = value.toBool() ? 1 : 0;
        break;
    }

    UsbHidDevice *dev = ((Termometer300*)QObject::parent())->getDevice();

    if (!dev->seek(MapItem->offsset))
        return false;

    if (!dev->write(buff, MapItem->size))
        return false;

    forcerereadData(index);
    return true;
}

QVariant DeviceSettingsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex DeviceSettingsModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

int DeviceSettingsModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

QModelIndex DeviceSettingsModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

void DeviceSettingsModel::tryCashValues() const
{
    if (!valuesCashed)
    {
        UsbHidDevice *dev = ((Termometer300*)QObject::parent())->getDevice();
        if (dev->seek(memoryMap[0].offsset))
            if (dev->read((char*)&cashedSettings, sizeof(sSettings)) == sizeof(sSettings))
            {
                LOG_DEBUG("Settings Cashed");
                valuesCashed = true;
            }
    }
}

void DeviceSettingsModel::setupModelData(TreeItem *root)
{
    for(int i = 0; i < sizeof(memoryMap) / sizeof(memoryMapItem); ++i)
    {
        const memoryMapItem* item = &memoryMap[i];
        QStringList path_chanks = item->path.split("/");

        TreeItem* parent = root;

        foreach(const QString& chank, path_chanks)
        {
            TreeItem *t = NULL;
            foreach(TreeItem *child, *parent->getChildItems())
                if (child->path() == chank)
                {
                    t = child;
                    break;
                }
            if (!t)
            {
                // create node
                QVariantList content;
                if (path_chanks.indexOf(chank) == path_chanks.size() - 1)
                    content << item->name << QByteArray((char*)&item, sizeof(memoryMapItem*));
                else
                    content << item->groupName << -1;

                t = new TreeItem(chank, content, parent);
            }
            parent = t;
        }
    }
}

QVariant DeviceSettingsModel::value(DeviceSettingsModel::memoryMapItem *item) const
{
    if (item)
    {
        uint8_t * p = (uint8_t*)&cashedSettings + (item->offsset & 0xffff);
        switch(item->type)
        {
        case DeviceSettingsModel::FLOAT:
            return *((float*)p);
        case DeviceSettingsModel::UINT8_T:
            return *((uint8_t*)p);
        case DeviceSettingsModel::INT8_T:
            return *((int8_t*)p);
        case DeviceSettingsModel::UINT16_T:
            return *((uint16_t*)p);
        case DeviceSettingsModel::BOOL:
            return (bool)(*p);
        }
    }
    else
        return QVariant();
}

void DeviceSettingsModel::forcerereadData(const QModelIndex &index)
{
    valuesCashed = false;
    emit dataChanged(index, index);
}
