#include <QVBoxLayout>
#include <QPointF>
#include <QLinearGradient>

#include <math.h>

#include <mylog.h>

#include <qwt_plot_barchart.h>
#include <qwt_symbol.h>
#include <qwt_column_symbol.h>
#include <qwt_plot_layout.h>

#include "splinecurve.h"

#include "Graph.h"

Graph::Graph(QModelIndex *index) :
    QwtPlot(NULL)
{
    referenceIndex = index;

    setWindowFlags(Qt::FramelessWindowHint | Qt::Tool | Qt::WindowStaysOnTopHint);
    setAttribute(Qt::WA_TranslucentBackground);
    setStyleSheet("background-color: qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, stop:0 #E8E1B7, stop:1 #B7BEE8);"
                  "border: 2px solid; border-radius: 3px; border-color: #1376AB;");

    this->enableAxis(QwtPlot::yLeft, false);
    this->enableAxis(QwtPlot::yRight, false);
    this->enableAxis(QwtPlot::xBottom, false);

    SplineCurve* curve;

    curve = buildCurve(trUtf8("Температура"), QColor(Qt::red), QwtSymbol::Ellipse, QwtPlot::yLeft);
    curve->attach(this);
    curve->setZ(1);
    curves.append(curve);

    curve = buildCurve(trUtf8("Давление"), QColor(Qt::black), QwtSymbol::Ellipse, QwtPlot::yRight);
    curve->attach(this);
    curve->setZ(0);
    curves.append(curve);
}

Graph::~Graph()
{
}

QModelIndex *Graph::index() const
{
    return referenceIndex;
}

void Graph::SetData(const QVector<QPointF> TPH[])
{
    for (int i = 0; i < 2; ++i)
        curves[i]->setSamples(TPH[i]);

    if (TPH[0].size() > 1)
        this->setAxisScale(QwtPlot::xBottom, TPH[0].at(0).x(), TPH[0].last().x());

    this->replot();
}

SplineCurve *Graph::buildCurve(const QString &name, const QColor &color, const int style, const int y_axis)
{
    QPen pen = QPen(color);
    pen.setWidth(3);
    QwtSymbol* symbol = new QwtSymbol;
    symbol->setStyle((QwtSymbol::Style)style);
    symbol->setSize(5);
    symbol->setPen(pen);
    SplineCurve* curve = new SplineCurve(name);
    curve->setSymbol(symbol);
    curve->setPen(pen);
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setAxes(QwtPlot::xBottom, y_axis);
    curve->setStyle(QwtPlotCurve::Steps);

    return curve;
}
