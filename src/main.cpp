#include <QApplication>
#include <QIcon>
#include <QMessageBox>
#include <QList>
#include <QTabWidget>
#include <qalgorithms.h>

#include <libusb.h>

#include <stdexcept>
#include <stdio.h>

#include <mylog/mylog.h>

#include "Settings.h"
#include "realtimegraphwindow.h"
#include "Termometer300.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("Termometer300");
    app.setWindowIcon(QIcon(":/res/weather_icon_hot_thumb1.png"));

    Settings settings(argc, argv);

    MyLog::myLog log;

#ifndef NDEBUG
    log.setLogLevel(MyLog::LOG_DEBUG);
#endif

    QList<Termometer300 *> devices;

    libusb_context *libusbContext = NULL;
    libusb_device **list = NULL;
    int rc = 0;
    ssize_t count = 0;

    rc = libusb_init(&libusbContext);
    if (rc != 0)
    {
        LOG_FATAL("Failed to init libmodbus!");
        return -1;
    }

    count = libusb_get_device_list(libusbContext, &list);
    if (count <= 0)
    {
        LOG_FATAL("Failed to get devices list!");
        return -1;
    }

    for (size_t idx = 0; idx < count; ++idx) {
        libusb_device *device = list[idx];
        libusb_device_descriptor desc;

        rc = libusb_get_device_descriptor(device, &desc);
        if (rc != 0)
        {
            LOG_ERROR("Failed to get device descriptor");
            continue;
        }

        if (desc.idVendor == SCTB_ELPA_VID && desc.idProduct == TERMOMETER300_PID)
        {
            Termometer300 * d;
            try
            {
                d = new Termometer300(device);
            }
            catch (std::runtime_error e)
            {
                LOG_ERROR(QObject::trUtf8("Failed to open device at 0x%1").arg((size_t)device));
            }
            devices.append(d);
        }
        LOG_INFO(QObject::trUtf8("Omitted %1:%2").arg(
                     desc.idVendor, 4, 16, QChar('0')).arg(
                     desc.idProduct, 4, 16, QChar('0')));
    }

    libusb_free_device_list(list, 1);

    if (devices.isEmpty())
    {
        QMessageBox::critical(NULL, QObject::trUtf8("Ошибка"),
                              QObject::trUtf8("Устройств не найдено, проверьте соединение!"),
                              QMessageBox::Ok);

        return -1;
    }
    else
        LOG_INFO(QObject::trUtf8("Found %1 devise(s)").arg(devices.count()));

    QTabWidget* tabedWindow = new QTabWidget(NULL); // Окно верхнего уровня.
    tabedWindow->setWindowTitle(QObject::trUtf8("Показания датчиков"));
    foreach (Termometer300* d, devices)
    {
        RealtimeGraphWindow *w = new RealtimeGraphWindow(d);
        QObject::connect(d, SIGNAL(dataRessived(Termometer300::Mesure)), w, SLOT(newMesure(Termometer300::Mesure)));

        tabedWindow->addTab(w, QObject::trUtf8("Термометр 0x%1").arg((size_t)d->getDevice()->getDeviceHandle()));
    }

    tabedWindow->show();

    int res = app.exec();

    qDeleteAll(devices);

    libusb_exit(libusbContext);

    return res;
}
