# - Try to find myusbhiddevice
# Once done this will define
#
#  MYUSBHIDDEVICE_FOUND - system has MYUSBHIDDEVICE
#  MYUSBHIDDEVICE_INCLUDE_DIR - the MYUSBHIDDEVICE include directory
#  MYUSBHIDDEVICE_LIBRARIES - Link these to use MYUSBHIDDEVICE

# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if (MYUSBHIDDEVICE_INCLUDE_DIR AND MYUSBHIDDEVICE_LIBRARIES)

  # in cache already
  set(SETTINGSCMDLINE_FOUND TRUE)
  message(STATUS "Found libmyusbhiddevice: ${MYUSBHIDDEVICE_LIBRARIES}")

else (MYUSBHIDDEVICE_INCLUDE_DIR AND MYUSBHIDDEVICE_LIBRARIES)

  find_path(MYUSBHIDDEVICE_INCLUDE_DIR usbhiddevice.h
    PATH_SUFFIXES myusbhiddevice
	PATHS
		/usr/include
		/usr/local/include
		$ENV{WD}/../include
		$ENV{WD}/../local/include
  )

  find_library(MYUSBHIDDEVICE_LIBRARIES_DEBUG
            NAMES
                myusbhiddeviced
            PATHS
                /usr/lib
		/usr/bin
		/usr/local/lib
		/usr/local/bin
            )

  find_library(MYUSBHIDDEVICE_LIBRARIES_RELEASE
            NAMES
                myusbhiddevice
            PATHS
                /usr/lib
                /usr/bin
                /usr/local/lib
                /usr/local/bin
            )
   SET(MYUSBHIDDEVICE_LIBRARIES
      debug ${MYUSBHIDDEVICE_LIBRARIES_DEBUG}
      optimized ${MYUSBHIDDEVICE_LIBRARIES_RELEASE}
      )


 set(CMAKE_REQUIRED_INCLUDES ${MYUSBHIDDEVICE_INCLUDE_DIR})
 set(CMAKE_REQUIRED_LIBRARIES ${MYUSBHIDDEVICE_LIBRARIES})

   if(MYUSBHIDDEVICE_INCLUDE_DIR AND MYUSBHIDDEVICE_LIBRARIES)
    set(SETTINGSCMDLINE_FOUND TRUE)
  else (MYUSBHIDDEVICE_INCLUDE_DIR AND MYUSBHIDDEVICE_LIBRARIES)
    set(SETTINGSCMDLINE_FOUND FALSE)
  endif(MYUSBHIDDEVICE_INCLUDE_DIR AND MYUSBHIDDEVICE_LIBRARIES)

  if (SETTINGSCMDLINE_FOUND)
    if (NOT SETTINGSCMDLINE_FIND_QUIETLY)
      message(STATUS "Found libmyusbhiddevice: ${MYUSBHIDDEVICE_LIBRARIES}")
    endif (NOT SETTINGSCMDLINE_FIND_QUIETLY)
  else (SETTINGSCMDLINE_FOUND)
    if (SETTINGSCMDLINE_FIND_REQUIRED)
      message(FATAL_ERROR "libmyusbhiddevice not found. Please install it.")
    endif (SETTINGSCMDLINE_FIND_REQUIRED)
  endif (SETTINGSCMDLINE_FOUND)

  mark_as_advanced(MYUSBHIDDEVICE_INCLUDE_DIR MYUSBHIDDEVICE_LIBRARIES)

endif (MYUSBHIDDEVICE_INCLUDE_DIR AND MYUSBHIDDEVICE_LIBRARIES)
